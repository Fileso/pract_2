﻿namespace Filev;

public class Computer
{
    private Os _os;

    public void Launch(string osName, Component component)
    {
        _os = Os.GetInstance(osName);
        _os.InstanceFileSystem(component);
    }

    public void InstanceFileSystem(Component component)
    {
        _os.InstanceFileSystem(component);
    }

    public void ShowComponent()
    {
        _os.ShowComponent();
    }
}

public class Os
{
    private Component _component;
    private static Os _instance;
    private string _name;

    private Os(string name)
    {
        _name = name;
    }

    public static Os GetInstance(string name)
    {
        if (_instance == null)
        {
            _instance = new Os(name);
        }

        return _instance;
    }

    public void InstanceFileSystem(Component component)
    {
        _component = component;
    }

    public void ShowComponent()
    {
        Console.WriteLine(_name);
        _component.Print();
    }
}