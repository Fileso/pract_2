﻿namespace Filev;

class Program
{
    static void Main(string[] args)
    {
        Component fileSystem = new Directory("Файловая система");
        Component diskC = new Directory("Диск С");
        Component pngFile = new File("12345.png");
        Component docxFile = new File("Document.docx");
        diskC.Add(pngFile);
        diskC.Add(docxFile);
        fileSystem.Add(diskC);
        
        Computer computer = new Computer();
        computer.Launch("Windows 11", fileSystem);
        computer.ShowComponent();

        Console.WriteLine("\n-----------------\n");
        computer.Launch("Windows 10", fileSystem);
        computer.ShowComponent();
        Console.Read();
    }
}